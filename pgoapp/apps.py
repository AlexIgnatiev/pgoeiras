from django.apps import AppConfig


class PgoappConfig(AppConfig):
    name = 'pgoapp'
