from django import forms

from pgoapp.models import Player


class PlayerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PlayerForm, self).__init__(*args, **kwargs)
        for _, f in self.fields.items():
            f.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Player
        fields = ['nickname', 'code', 'team', 'level']
