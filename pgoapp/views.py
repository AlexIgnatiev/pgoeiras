from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from pgoapp.forms import PlayerForm
from pgoapp.models import Player


def table_view(request):
    context = {
        'players': Player.objects.filter(active=True).order_by('creation'),
        'form': PlayerForm()
    }
    return render(request, "table.html", context=context)


@csrf_exempt
def submit(request):
    # TODO: refactor to use django forms
    if request.is_ajax():
        action = request.POST.get('action', 'create')

        nickname = request.POST['nickname']
        code = request.POST.get('code', None)
        level = request.POST.get('level', None)

        if action == 'create':
            player, created = Player.objects.get_or_create(nickname=nickname, defaults={'code': code, 'level': level})
            if not created:
                player.active = True
        else:
            player = Player.objects.get(nickname=nickname)
            if action == 'delete':
                player.active = False
            elif action == 'edit':
                player.active = True
                player.code = code
                player.level = level
            else:
                player.active = True
        player.save()
        data = {'status': 'ok'}
        return JsonResponse(data)


@csrf_exempt
def submit_form(request):
    action = request.POST.get('action', 'create')
    nickname = request.POST['nickname']
    if action == 'create' or action == 'edit':
        form = PlayerForm(request.POST)
        if form.is_valid():
            player = Player.create_or_activate(form)
            player.save()
    elif action == 'delete':
        Player.deactivate(nickname)
    else:
        Player.activate(nickname)

    return JsonResponse({'status': 'ok'})


# TODO: make this static
def faq(request):
    return render(request, "faq.html")
