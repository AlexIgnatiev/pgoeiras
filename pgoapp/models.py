from django.db import models


class Player(models.Model):
    TEAM_CHOICES = (
        ('b', ''),
        ('i', 'Instinct'),
        ('m', 'Mystic'),
        ('v', 'Valor')
    )

    nickname = models.CharField(max_length=128)
    code = models.CharField(max_length=12)
    team = models.CharField(max_length=2, choices=TEAM_CHOICES, default='b')
    level = models.PositiveSmallIntegerField(default=0)
    active = models.BooleanField(default=True)
    creation = models.DateTimeField(auto_now_add=True)
    last_edit = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nickname

    @classmethod
    def create_or_activate(cls, form):
        try:
            player = cls.objects.get(nickname=form.cleaned_data['nickname'])
        except cls.DoesNotExist:
            player = form.save(commit=False)

        player.level = form.cleaned_data['level']
        player.team = form.cleaned_data['team']
        player.code = form.cleaned_data['code']
        player.active = True
        return player

    @classmethod
    def activate(cls, nickname):
        p = cls.objects.get(nickname=nickname)
        p.active = True
        p.save()

    @classmethod
    def deactivate(cls, nickname):
        p = cls.objects.get(nickname=nickname)
        p.active = False
        p.save()
