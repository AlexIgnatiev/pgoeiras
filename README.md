##PG-Oeiras

Requirements:

  - python3.6
  - pip
  - (optional) python-virtualenv
  
  

Run locally (on Linux. Windows should also work in equivalent fashion...for now):

1. (Optional) create virutalenv `virtualenv -p python3.6 venv`

2. (Optional) activate virtualenv `source venv/bin/activate`

3. install python dependencies `pip install -r requirements.txt`

4. create database and tables `python manage.py migrate`

5. run the server on localhost `python manage.py runserver`

TODO:
 - Docker-ize the project.
 - tests